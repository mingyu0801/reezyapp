import kue from 'kue'

import app from './src/server'
import { PORT } from './src/config'
import SchedulerController from './src/modules/scheduler/scheduler.controller'
import QueueController from './src/modules/queue/queue.controller'

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))

const queue = new QueueController()
kue.app.listen(7000)
queue.run()

const scheduler = new SchedulerController()
scheduler.run()
