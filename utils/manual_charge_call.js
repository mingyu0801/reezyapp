import prompts from 'prompts'

import { createChargeTasks, selectLeasesToBePaid } from '../src/modules/scheduler/scheduler.controller'

const main = async () => {
  const dueLeases = await selectLeasesToBePaid()
  const response = await prompts({
    type: 'text',
    name: 'consent',
    message: `The following leases will be charged: ${dueLeases.map(lease => lease.id).join()}.\nPlease confirm by typing "proceed".`
  })
  if (response.consent === 'proceed') {
    await createChargeTasks(dueLeases)
    console.log('Done.')
  }
  console.log('Press Ctrl+C to exit.')
}

main()
