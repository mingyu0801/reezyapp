require('babel-register')

const path = require('path')

const DB_URI = process.env.MYSQL_URI || 'mysql://root:coder@localhost/reezy'

const BASE_PATH = path.join(__dirname, 'src', 'database')

module.exports = {
  client: 'mysql',
  connection: DB_URI,
  migrations: {
    directory: path.join(BASE_PATH, 'migrations'),
  },
  seeds: {
    directory: path.join(BASE_PATH, 'seeds'),
  },
}
