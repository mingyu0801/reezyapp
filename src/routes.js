
import compose from 'koa-compose'
import Router from 'koa-router'

import AuthRouter from './modules/auth/auth.routes'
import PropertiesRouter from './modules/properties/properties.routes'
import LeasesRouter from './modules/leases/leases.routes'
import PlaidRoutes from './modules/plaid/plaid.routes'
// import ChargeRouter from './modules/charge/charge.routes'
// import DepositRouter from './modules/deposit/deposit.routes'
// import PaymentRouter from './modules/payment/payment.routes'
// import WebhookRouter from './modules/webhook/webhook.routes'
// import ChatbotRouter from './modules/chatbot/chatbot.routes'

const router = new Router()

router.get('/', async (ctx) => {
  ctx.body = 'Reezy API'
})

router.use('/api/v1/auth', AuthRouter.routes(), AuthRouter.allowedMethods())
router.use('/api/v1/properties', PropertiesRouter.routes(), PropertiesRouter.allowedMethods())
router.use('/api/v1/leases', LeasesRouter.routes(), LeasesRouter.allowedMethods())
// Plaid integration routes
router.use('/api/v1/plaid', PlaidRoutes.routes(), PlaidRoutes.allowedMethods())

// Disabled routes
// router.use('/api/v1/charge', ChargeRouter.routes(), ChargeRouter.allowedMethods())
// router.use('/api/v1/deposit', DepositRouter.routes(), DepositRouter.allowedMethods())
// router.use('/api/v1/payment', PaymentRouter.routes(), PaymentRouter.allowedMethods())
// router.use('/api/v1/webhook', WebhookRouter.routes(), WebhookRouter.allowedMethods())
// router.use('/api/v1/chatbot', ChatbotRouter.routes(), ChatbotRouter.allowedMethods())

router.get('*', async (ctx) => {
  ctx.body = { status: 404 }
})

export default function routes() {
  return compose(
    [
      router.routes(),
      router.allowedMethods(),
    ]
  )
}
