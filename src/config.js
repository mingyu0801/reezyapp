
export const EXTERNAL_API_TOKEN = process.env.EXTERNAL_API_TOKEN || ''
export const VERSAPAY_API_URL = process.env.VERSAPAY_API_URL || 'https://demo.versapay.com'

// Demo account credentials by default
export const VERSAPAY_API_TOKEN = process.env.VERSAPAY_API_TOKEN || ''
export const VERSAPAY_API_KEY = process.env.VERSAPAY_API_KEY || ''

export const VERSAPAY_FUND_TOKEN = process.env.VERSAPAY_FUND_TOKEN || ''
export const VERSAPAY_WEBHOOK_KEY = process.env.VERSAPAY_WEBHOOK_KEY || ''

export const VERSAPAY_TRANSACTION_STATUS = {
  IN_PROGRESS: 'in_progress',
  ERROR: 'error',
  NSFED: 'nsfed',
  CANCELLED: 'cancelled',
  COMPLETED: 'completed',
  COMPLETED_BUT_NSFED: 'completed_but_nsfed',
}

export const VERSAPAY_TRANSACTION_TYPE = {
  DIRECT_DEBIT: 'direct_debit',
  DIRECT_CREDIT: 'direct_credit',
}

export const PAYMENT_STATUS = {
  INITIATED: 'INITIATED',
  CHARGE_IN_PROGRESS: 'CHARGE_IN_PROGRESS',
  CHARGED: 'CHARGED',
  CHARGE_FAILED: 'CHARGE_FAILED',
  SEND_IN_PROGRESS: 'SEND_IN_PROGRESS',
  SENT: 'SENT',
  SEND_FAILED: 'SEND_FAILED',
}

export const LEASE_STATUS = {
  PENDING: 'PENDING',
  ACTIVE: 'ACTIVE',
  TERMINATED: 'TERMINATED',
}

export const BILLING_CYCLE = {
  DAILY: 'DAILY', // for testing purposes
  WEEKLY: 'WEEKLY',
  BI_WEEKLY: 'BI_WEEKLY',
  MONTHLY: 'MONTHLY',
}

export const PAYMENT_METHOD_TYPE = {
  EFT: 'EFT',
  // CC: 'CC',
}

export const DB_URI = process.env.DATABASE_URL || 'postgres://postgres:12345678@127.0.0.1:5432/reezy'
export const DB_URI_TEST = process.env.TEST_DATABASE_URL || 'postgres://postgres:12345678@127.0.0.1:5432/reezy-test'
export const REDIS_URI = process.env.REDIS_URL || 'redis://localhost:6379'

export const IS_HTTPS = ['production', 'staging'].includes(process.env.NODE_ENV)

export const REEZY_URL = process.env.REEZY_URL || ''
export const PORT = process.env.PORT || 3000
export const REEZY_API_URL = `http://localhost:${PORT}`

export const MIN_PASSWORD_LENGTH = 8
export const MAX_PASSWORD_LENGTH = 25

export const SOCIAL_TYPE = {
  FB: 'FB',
}

export const DIALOGFLOW_INTENT = {
  SEE_PROPERTIES: 'See properties',
  SIGNUP_PHONE: 'signup.phone',
  SIGNUP_PHONE_VERIFY: 'signup.phone.verify',
}


export const PLAID = {
  CLIENT_ID: process.env.PLAID_CLIENT_ID || '5bf20372ec24fd001207d6cd',
  SECRET: process.env.PLAID_SECRET || 'b5558f3e8b1c7e4e3a461241929e34',
  PUBLIC_KEY: process.env.PLAID_PUBLIC_KEY || '9f606f519ad3aa4acba74e33d121fc',
  ENV: process.env.PLAID_ENV || 'sandbox',
  URL: process.env.PLAID_URL || 'https://sandbox.plaid.com',
}
