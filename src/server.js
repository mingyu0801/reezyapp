import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import logger from 'koa-logger'
import passport from 'koa-passport'
import session from 'koa-session'
import cors from '@koa/cors'

import './modules/auth/strategies'
import routes from './routes'

const app = new Koa()

// sessions
app.keys = ['jkhsdfSA343HJweu']
app.use(session({
  domain: process.env.SESSION_DOMAIN,
}, app))

// cors
app.use(cors({
  origin: process.env.REEZY_APP_URL || '',
  allowMethods: 'GET,POST,PUT',
  credentials: true,
}))

app.use(logger())
app.use(bodyParser())

app.use(passport.initialize())
app.use(passport.session())

app.use(routes())


export default app
