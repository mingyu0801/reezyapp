
import Router from 'koa-router'
import Controller from './leases.controller'
import { authMiddleware } from '../../common'

const router = new Router()
const controller = new Controller()

router.get('/', authMiddleware, async (ctx) => {
  await controller.list(ctx)
})

router.post('/', authMiddleware, async (ctx) => {
  await controller.create(ctx)
})

router.put('/:leaseId', authMiddleware, async (ctx) => {
  await controller.update(ctx)
})

export default router
