import knex from '../../database/connection'
import { BILLING_CYCLE, LEASE_STATUS } from '../../config'

export default class LeasesController {
  async list(ctx) {
    const { user } = ctx.state

    const leases = await knex('lease_agreements')
      .innerJoin('properties', 'properties.id', 'lease_agreements.property_id')
      .where({
        'lease_agreements.tenant_id': user.id,
        'lease_agreements.status': LEASE_STATUS.ACTIVE,
      })
      .select(['lease_agreements.*', 'properties.address'])

    ctx.status = 200
    ctx.body = {
      success: true,
      response: leases,
    }
  }

  async create(ctx) {
    const { propertyId, tenantId, startDate, endDate, autoExtension, rentAmount, billingCycle } = ctx.request.body
    const { user } = ctx.state

    if (!propertyId || !tenantId || !startDate || !endDate || !rentAmount || !billingCycle) {
      ctx.throw(400, 'Missing required parameters')
    }

    const property = await knex('properties').where('id', propertyId).first('landlord_id')
    if (!property) {
      ctx.throw(412, 'The property doesn\'t exist')
    }

    if (property.landlord_id !== user.id) {
      ctx.throw(412, 'Only the landlord of the property can initiate a new lease agreement')
    } else if (property.landlord_id === tenantId) {
      ctx.throw(412, 'The landlord of the property can\'t be a tenant of the same property')
    }

    const tenant = await knex('users').where('id', tenantId).first('id')
    if (!tenant) {
      ctx.throw(412, 'The specified tenant doesn\'t exist')
    }

    const dateFormatRegExp = /^\d{4}-\d{2}-\d{2}$/

    if (!dateFormatRegExp.test(startDate) || !dateFormatRegExp.test(endDate)) {
      ctx.throw(412, 'The correct format for startDate and endDate is YYYY-MM-DD')
    }

    // !!! Add Rule to disallow start date on 28-31 day on month

    if (!Number.isInteger(rentAmount) || rentAmount < 1) {
      ctx.throw(412, 'rentAmount must be a decimal number and greater than zero')
    }

    if (!Object.values(BILLING_CYCLE).includes(billingCycle)) {
      ctx.throw(412, `billingCycle must be one of the following values: ${Object.values(BILLING_CYCLE).join(', ')}`)
    }
    // const startDateMoment = moment(startDate).utc()
    // const endDateMoment = moment(endDate).utc()
    // const currMoment = moment().utc()
    // if (!startDateMoment.isAfter(currMoment) || endDateMoment.isAfter(currMoment)) {
    //   ctx.throw(412, 'startDate and endDate must be in the future')
    // }

    const agreement = await knex('lease_agreements').returning('id').insert({
      property_id: propertyId,
      tenant_id: tenantId,
      start_date: startDate,
      end_date: endDate,
      auto_extension: autoExtension || true,
      rent_amount: rentAmount,
      billing_cycle: billingCycle,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })
    const agreementId = agreement && agreement[0]

    if (!agreementId) {
      ctx.throw(500, 'Unable to create a lease agreement')
    }

    ctx.status = 200
    ctx.body = {
      success: true,
      response: {
        id: agreementId,
      },
    }
  }

  async update(ctx) {
    const { user } = ctx.state
    const { id, status, rentAmount, billingCycle } = ctx.request.body

    if (!id || (!status && !rentAmount && !billingCycle)) {
      ctx.throw(400, 'Missing required parameters')
    }

    if (status && !Object.values(LEASE_STATUS).includes(status)) {
      ctx.throw(412, `status must be one of the following values: ${Object.values(LEASE_STATUS).join(', ')}`)
    }

    if (rentAmount && (!Number.isInteger(rentAmount) || rentAmount < 1)) {
      ctx.throw(412, 'rentAmount must be a decimal number and greater than zero')
    }

    if (billingCycle && !Object.values(BILLING_CYCLE).includes(billingCycle)) {
      ctx.throw(412, `billingCycle must be one of the following values: ${Object.values(BILLING_CYCLE).join(', ')}`)
    }

    const lease = await knex('lease_agreements')
      .innerJoin('properties', 'properties.id', 'lease_agreements.property_id')
      .where('lease_agreements.id', id).first(['tenant_id', 'landlord_id'])
    if (!lease) {
      ctx.throw(412, 'The lease agreement doesn\'t exist')
    }

    const updateQuery = {}
    if (status) {
      if (status === LEASE_STATUS.ACTIVE && user.id === lease.tenant_id) {
        updateQuery.status = status
      } else if (status !== LEASE_STATUS.ACTIVE && user.id === lease.landlord_id) {
        updateQuery.status = status
      }
    }

    if (rentAmount) {
      if (user.id !== lease.landlord_id) {
        ctx.throw(412, 'rentAmount can only be modified by the landlord')
      } else {
        updateQuery.rent_amount = rentAmount
        updateQuery.status = LEASE_STATUS.PENDING
      }
    }

    if (billingCycle) {
      if (user.id !== lease.landlord_id) {
        ctx.throw(412, 'billingCycle can only be modified by the landlord')
      } else {
        updateQuery.billing_cycle = billingCycle
        updateQuery.status = LEASE_STATUS.PENDING
      }
    }

    if (!Object.values(updateQuery).length) {
      ctx.throw(412, 'Don\'t have the permission to make this update to the lease')
    }

    await knex('lease_agreements').where('id', id).update(updateQuery)

    ctx.status = 200
    ctx.body = {
      success: true,
    }
  }
}
