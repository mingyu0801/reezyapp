import kue from 'kue'
import axios from 'axios'

import { REEZY_API_URL, REDIS_URI } from '../../config'

export const queueInstance = kue.createQueue({
  redis: REDIS_URI,
})

queueInstance.watchStuckJobs()

export default class QueueController {
  run() {
    queueInstance.process('chargeTenant', async ({ data }, done) => {
      try {
        await axios.post(`${REEZY_API_URL}/api/v1/charge`, {
          paymentId: data.payment_id,
        })
        done()
      } catch (error) {
        console.error('Reezy charge API request failed:', error.response.data)
      }
    })

    queueInstance.process('depositToLandlord', async ({ data }, done) => {
      try {
        await axios.post(`${REEZY_API_URL}/api/v1/deposit`, {
          paymentId: data.payment_id,
        })
        done()
      } catch (error) {
        console.error('Reezy deposit API request failed:', error.response.data)
      }
    })
  }
}
