import axios from 'axios'
import moment from 'moment'
import crypto from 'crypto'

import knex from '../../database/connection'
import {
  VERSAPAY_API_URL,
  VERSAPAY_API_KEY,
  VERSAPAY_API_TOKEN,
  VERSAPAY_FUND_TOKEN,
  PAYMENT_STATUS,
} from '../../config'


export default class DepositController {
  async index(ctx) {
    const { paymentId } = ctx.request.body

    if (!paymentId) {
      ctx.throw(400, 'Missing required parameters')
    }

    const payment = await knex('payments')
      .where('payments.id', paymentId)
      .join('lease_agreements', 'payments.lease_id', '=', 'lease_agreements.id')
      .first(['payments.amount', 'payments.lease_id', 'payments.deposit_status', 'payments.charge_status', 'lease_agreements.property_id'])

    if (!payment) {
      ctx.throw(412, 'The transaction doesn\'t exist')
    } else if ([PAYMENT_STATUS.SEND_FAILED, PAYMENT_STATUS.SEND_IN_PROGRESS, PAYMENT_STATUS.SENT].includes(payment.deposit_status) ||
      ![PAYMENT_STATUS.CHARGE_IN_PROGRESS, PAYMENT_STATUS.CHARGED].includes(payment.charge_status)) {
      ctx.throw(412, 'The transaction status is not valid, possibly it\'s already payed out')
    }

    const property = await knex('properties').where('id', payment.property_id).first(['landlord_id', 'address'])
    if (!property) {
      ctx.throw(412, 'The property doesn\'t exist')
    }

    const paymentMethod = await knex('payment_methods').where('user_id', property.landlord_id).first('id')
    if (!paymentMethod) {
      ctx.throw(412, 'The landlord doesn\'t have any payment method set up')
    }

    const versaPayToken = await knex('versapay_tokens').where('payment_method_id', paymentMethod.id).first('fund_token')
    if (!versaPayToken) {
      ctx.throw(412, 'The landlord doesn\'t have VersaPay fund token set up')
    }

    const landlord = await knex('users').where('id', property.landlord_id).first()
    if (!landlord) {
      ctx.throw(412, 'The landlord information doesn\'t exist')
    }

    const uniqueReference = crypto.createHash('md5').update(`DEPOSIT_${property.landlord_id}_${payment.lease_id}_${paymentId}_${process.env.NODE_ENV}`).digest('hex')
    let paymentSuccess = true
    let versaPayResponse
    try {
      versaPayResponse = await axios({
        method: 'post',
        url: `${VERSAPAY_API_URL}/api/transactions`,
        data: {
          transaction_type: 'direct_credit',
          amount_in_cents: payment.amount * 100,
          email: landlord.email,
          message: `Rent payment for the lease #${payment.lease_id}. #${paymentId}`,
          process_on: moment().format('YYYY-MM-DD'),
          unique_reference: uniqueReference,
          fund_token: VERSAPAY_FUND_TOKEN,
          to_fund_token: versaPayToken.fund_token,
          first_name: landlord.first_name,
          last_name: landlord.last_name,
          memo: `Rent for ${property.address}`,
          auto_withdraw: false,
        },
        auth: {
          username: VERSAPAY_API_TOKEN,
          password: VERSAPAY_API_KEY,
        },
      })
    } catch (error) {
      console.error('VersaPay API direct_credit request failed:', error.response.data)
      paymentSuccess = false
    }

    if (paymentSuccess) {
      try {
        await knex('payments').where('id', paymentId).update({
          deposit_status: PAYMENT_STATUS.SEND_IN_PROGRESS,
          updated_at: knex.raw('now()'),
        })
        await knex('versapay_transactions').insert({
          token: versaPayResponse.data.token,
          state: versaPayResponse.data.state,
          payment_id: paymentId,
          created_at: knex.raw('now()'),
          updated_at: knex.raw('now()'),
        })
      } catch (e) {
        console.error('Failed to log VersaPay transaction', e)
      }
    }

    ctx.status = 200
    ctx.body = 'Success'
  }
}
