import knex from '../../database/connection'

export default class PropertiesController {
  async list(ctx) {
    const { user } = ctx.state

    let properties
    try {
      properties = await knex('properties')
        .leftJoin('lease_agreements', 'lease_agreements.property_id', 'properties.id')
        .leftJoin('users', 'users.id', 'lease_agreements.tenant_id')
        .where('landlord_id', user.id)
        .select(['properties.address', 'lease_agreements.end_date', 'users.first_name', 'users.last_name'])
    } catch (e) {
      ctx.throw(500, 'Unable to query properties')
    }

    ctx.status = 200
    ctx.body = {
      success: true,
      response: properties,
    }
  }

  async create(ctx) {
    const { address } = ctx.request.body
    const { user } = ctx.state

    if (!address) {
      ctx.throw(400, 'Missing required parameters')
    }

    const property = await knex('properties').returning('id').insert({
      landlord_id: user.id,
      address,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })
    const propertyId = property && property[0]

    if (!propertyId) {
      ctx.throw(500, 'Unable to create a property')
    }

    ctx.status = 200
    ctx.body = {
      success: true,
      response: {
        id: propertyId,
      },
    }
  }

  async showLeases(ctx) {
    const { user } = ctx.state
    const { propertyId } = ctx.params

    if (!propertyId) {
      ctx.throw(400, 'Missing required parameters')
    }

    const property = await knex('properties').where('id', propertyId).first('landlord_id')
    if (!property) {
      ctx.throw(412, 'The property doesn\'t exist')
    }

    if (property.landlord_id !== user.id) {
      ctx.throw(412, 'Only the landlord of the property can see its lease agreements')
    }

    const leases = await knex('lease_agreements').where('property_id', propertyId).select()

    ctx.status = 200
    ctx.body = {
      success: true,
      response: leases,
    }
  }
}
