
import Router from 'koa-router'
import Controller from './properties.controller'
import { authMiddleware } from '../../common'

const router = new Router()
const controller = new Controller()

router.get('/', authMiddleware, async (ctx) => {
  await controller.list(ctx)
})

router.post('/', authMiddleware, async (ctx) => {
  await controller.create(ctx)
})

router.get('/:propertyId/leases', authMiddleware, async (ctx) => {
  await controller.showLeases(ctx)
})

export default router
