
import Router from 'koa-router'
import Controller from './payment.controller'
import { authMiddleware } from '../../common'

const router = new Router()
const controller = new Controller()

router.get('/', authMiddleware, async (ctx) => {
  await controller.showPaymentMethods(ctx)
})
router.post('/', authMiddleware, async (ctx) => {
  await controller.addPaymentMethod(ctx)
})
router.delete('/', authMiddleware, async (ctx) => {
  await controller.deletePaymentMethod(ctx)
})
router.get('/history', authMiddleware, async (ctx) => {
  await controller.paymentHistory(ctx)
})


export default router
