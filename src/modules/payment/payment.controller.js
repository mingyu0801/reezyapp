import axios from 'axios'
import moment from 'moment'
import crypto from 'crypto'

import knex from '../../database/connection'
import {
  VERSAPAY_API_URL,
  VERSAPAY_API_KEY,
  VERSAPAY_API_TOKEN,
  VERSAPAY_FUND_TOKEN,
  PAYMENT_METHOD_TYPE,
  PAYMENT_STATUS,
} from '../../config'


export default class PaymentController {
  async verifyEFT(ctx, institutionNumber, branchNumber, accountNumber) {
    const { user } = ctx.state
    const uniqueReference = crypto.createHash('md5').update(`VERIFICATIONv1.06${user.id}${institutionNumber}${branchNumber}${accountNumber}${process.env.NODE_ENV}`).digest('hex')
    let versaPayResponse
    try {
      versaPayResponse = await axios({
        method: 'post',
        url: `${VERSAPAY_API_URL}/api/transactions`,
        data: {
          transaction_type: 'direct_debit',
          amount_in_cents: 100,
          email: user.email,
          message: 'Payment method verification',
          process_on: moment().format('YYYY-MM-DD'),
          unique_reference: uniqueReference,
          fund_token: VERSAPAY_FUND_TOKEN,
          institution_number: institutionNumber,
          branch_number: branchNumber,
          account_number: accountNumber,
          first_name: user.first_name,
          last_name: user.last_name,
          memo: 'Verification',
          auto_withdraw: false,
        },
        auth: {
          username: VERSAPAY_API_TOKEN,
          password: VERSAPAY_API_KEY,
        },
      })
    } catch (error) {
      console.error('VersaPay API direct_debit verification request failed:', error.response.data)
      ctx.throw(412, 'Unable to verify the provided bank information')
    }

    if (!versaPayResponse.data || !versaPayResponse.data.from_fund_token) {
      ctx.throw(412, 'Unable to verify the provided bank information')
      console.log('VersaPay responded', versaPayResponse)
    }

    await knex('versapay_transactions').insert({
      token: versaPayResponse.data.token,
      state: versaPayResponse.data.state,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })

    const paymentMethod = await knex('payment_methods').returning('id').insert({
      user_id: user.id,
      type: PAYMENT_METHOD_TYPE.EFT,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })

    const paymentMethodId = paymentMethod && paymentMethod[0]
    if (!paymentMethodId) {
      ctx.throw(412, 'Unable to add a new payment method')
    }

    await knex('versapay_tokens').returning('id').insert({
      payment_method_id: paymentMethodId,
      fund_token: versaPayResponse.data.from_fund_token,
      account_number: accountNumber.toString().replace(/^\d*([\d]{4})$/, '$1'),
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })
  }

  async addPaymentMethod(ctx) {
    const { type, institutionNumber, branchNumber, accountNumber } = ctx.request.body

    if (!type) {
      ctx.throw(400, 'Missing required parameters')
    }

    if (!Object.values(PAYMENT_METHOD_TYPE).includes(type)) {
      ctx.throw(412, `type must be of the following values: ${Object.values(PAYMENT_METHOD_TYPE).join(', ')}`)
    }

    if (type === PAYMENT_METHOD_TYPE.EFT && (!institutionNumber || !branchNumber || !accountNumber)) {
      ctx.throw(400, 'Missing required parameters for EFT method')
    }

    if (type === PAYMENT_METHOD_TYPE.EFT) {
      await this.verifyEFT(ctx, institutionNumber, branchNumber, accountNumber)
    }

    ctx.status = 200
    ctx.body = {
      success: true,
    }
  }

  async showPaymentMethods(ctx) {
    const { user } = ctx.state

    const methods = await knex('payment_methods')
      .innerJoin('versapay_tokens', 'payment_methods.id', 'versapay_tokens.payment_method_id')
      .where('user_id', user.id)
      .select(['payment_methods.id', 'type', 'account_number'])

    ctx.status = 200
    ctx.body = {
      success: true,
      response: methods,
    }
  }

  async deletePaymentMethod(ctx) {
    const { user } = ctx.state
    const { id } = ctx.request.body

    if (!id) {
      ctx.throw(400, 'Missing required parameters')
    }

    const paymentMethod = await knex('payment_methods').where({
      id,
      user_id: user.id,
    }).first('type')

    if (!paymentMethod) {
      ctx.throw(404, 'Can\'t find the payment method')
    }

    await knex('payment_methods').where({
      id,
      user_id: user.id,
    }).del()

    if (paymentMethod.type === PAYMENT_METHOD_TYPE.EFT) {
      await knex('versapay_tokens').where('payment_method_id', id).del()
    }

    ctx.status = 200
    ctx.body = {
      success: true,
    }
  }

  async paymentHistory(ctx) {
    const { user } = ctx.state

    let payments
    try {
      payments = await knex('payments')
        .innerJoin('lease_agreements', 'lease_agreements.id', 'payments.lease_id')
        .innerJoin('properties', 'properties.id', 'lease_agreements.property_id')
        .where({
          'properties.landlord_id': user.id,
          'payments.deposit_status': PAYMENT_STATUS.SENT,
        })
        .orderBy('payments.created_at', 'desc')
        .select('payments.created_at', 'payments.amount', 'properties.address')
    } catch (e) {
      ctx.throw(500, 'Unable to fetch payment history')
    }

    ctx.status = 200
    ctx.body = {
      success: true,
      response: payments,
    }
  }
}
