import { WebhookClient } from 'dialogflow-fulfillment'
import Authy from 'authy'

import { DIALOGFLOW_INTENT, SOCIAL_TYPE } from '../../config'
import knex from '../../database/connection'

import PropertiesController from '../properties/properties.controller'
import AuthController from '../auth/auth.controller'

class Response {
  constructor(ctx) {
    this.ctx = ctx
  }

  status(code) {
    this.ctx.status = code
  }

  json(value) {
    console.log('json called', { value })
    this.ctx.body = value
  }
}

export default class ChatbotController {
  constructor() {
    this.property = new PropertiesController()
    this.auth = new AuthController()
    this.listProperties = this.listProperties.bind(this)

    this.authy = Authy(process.env.AUTHY_KEY)
  }

  async authUser(ctx) {
    const { originalDetectIntentRequest } = ctx.request.body

    if (!originalDetectIntentRequest.payload.data) {
      return false
    }

    const userSocialId = originalDetectIntentRequest.payload.data.sender.id

    let socialUser
    try {
      socialUser = await knex('user_social_ids').where({
        socialType: SOCIAL_TYPE.FB,
        user_social_id: userSocialId,
      }).first()
    } catch (e) {
      console.log('Internal error while trying to match social user')
      return false
    }
    if (!socialUser) {
      console.log('Social user not found')
      return false
    }

    let user
    try {
      user = await knex('users').where('id', socialUser.user_id).first()
    } catch (e) {
      console.log('Internal error while trying to match user')
      return false
    }
    if (!user) {
      console.log('User not found')
      return false
    }
    console.log('user', user)
    ctx.login(user)
    return true
  }

  async createUser(ctx) {
    const { queryResult, originalDetectIntentRequest } = ctx.request.body
    const phone = queryResult.parameters.phone
    
    if (!originalDetectIntentRequest.payload.data) {
      return false
    }

    const userSocialId = originalDetectIntentRequest.payload.data.sender.id
    const firstName = originalDetectIntentRequest.payload.data.sender.first_name
    const lastName = originalDetectIntentRequest.payload.data.sender.last_name
    
    this.auth.createUserFacebook()
  }

  async listProperties() {
    const res = await this.property.list()
    console.log({ res })
  }

  async sendCode(ctx, agent) {
    const { queryResult } = ctx.request.body
    const phone = queryResult.parameters.phone

    if (!phone) {
      return false;
    }

    const res = await new Promise((resolve, reject) => {
      this.authy.phones().verification_start(phone, '1', 'sms', (err, res) => {
        if (!err && res.success) {
          resolve(res)
        } else {
          reject(err)
        }
      })
    })

    if (res.success) {
      agent.setContext({
        name: 'signup.phone-followup',
        lifespan: 5,
        parameters: {
          phone,
        },
      })
    }
  }

  async verifyCode(ctx) {
    const { queryResult } = ctx.request.body
    const phone = queryResult.parameters.phone
    const code = queryResult.parameters.code

    console.log({ queryResult })
    return;
    if (!code) {
      return false;
    }

    authy.phones().verification_check(phone, '1', code, async (err, res) => {
      if (!err) {
        const createUserResponse = await reezy.createUser(userInfo.first_name, userInfo.last_name, `1${userStorage.phone}`, message.user)
        if (!createUserResponse.ok) {
          convo.gotoThread('error')
        } else {

        }
      }
    })
  }

  async index(ctx) {
    const res = new Response(ctx)
    console.log(ctx.request.body)
    await this.authUser(ctx)
    const agent = new WebhookClient({ request: ctx.request, response: res, debug: true })
    console.log(ctx.request.body.originalDetectIntentRequest.payload.data)

    let intentMap = new Map()
    intentMap.set(DIALOGFLOW_INTENT.SEE_PROPERTIES, this.listProperties)
    intentMap.set(DIALOGFLOW_INTENT.SIGNUP_PHONE, (agent) => this.sendCode(ctx, agent))
    intentMap.set(DIALOGFLOW_INTENT.SIGNUP_PHONE_VERIFY, () => this.verifyCode(ctx))
    intentMap.set(null, (agent) => {
      // agent.setFollowupEvent('Properties')
      // agent.add('Adding value')
    })
    agent.handleRequest(intentMap)
  }
}
