
import Router from 'koa-router'
import Controller from './chatbot.controller'

const router = new Router()
const controller = new Controller()

router.post('/', async (ctx) => {
  await controller.index(ctx)
})

export default router
