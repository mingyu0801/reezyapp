
import Router from 'koa-router'
import Controller from './webhook.controller'

const router = new Router()
const controller = new Controller()

router.post('/versapay', async (ctx) => {
  await controller.versaPay(ctx)
})

export default router
