import crypto from 'crypto'

import knex from '../../database/connection'
import {
  VERSAPAY_TRANSACTION_STATUS,
  VERSAPAY_TRANSACTION_TYPE,
  VERSAPAY_WEBHOOK_KEY,
  PAYMENT_STATUS,
  REEZY_URL,
} from '../../config'

// import { queueInstance } from '../queue/queue.controller'

export default class WebhookController {
  async versaPay(ctx) {
    const { transaction_type: transactionType } = ctx.request.body
    const webhookURL = `${REEZY_URL}/api/v1/webhook/versapay`

    const params = Object.assign({}, ctx.request.body)
    const originalSignature = params.signature
    delete params.signature

    const sortedKeyValues = Object.keys(params).sort().map(key => `${key}${params[key]}`).join('')
    const requestString = `POST\n${webhookURL}\n${sortedKeyValues}`

    const hmacBase64 = crypto.createHmac('sha256', VERSAPAY_WEBHOOK_KEY).update(requestString).digest('base64')
    const expectedSignature = encodeURIComponent(`${hmacBase64}\n`)

    if (originalSignature !== expectedSignature) {
      ctx.throw(401, 'Wrong signature')
    }

    const trans = await knex('versapay_transactions')
      .leftJoin('payments', 'payments.id', '=', 'versapay_transactions.payment_id')
      .where('versapay_transactions.token', params.token)
      .first(['payments.id', 'payments.charge_status', 'payments.deposit_status', 'versapay_transactions.token'])

    if (!trans) {
      // Unknown transaction return 200, do nothing
      ctx.status = 200
      ctx.body = {
        success: true,
        unknown: true,
      }
      return ctx
    }

    await knex('versapay_transactions').where('token', params.token).update('state', params.state)

    if (trans.id) {
      const versaPayFailedStatuses = [
        VERSAPAY_TRANSACTION_STATUS.ERROR,
        VERSAPAY_TRANSACTION_STATUS.NSFED,
        VERSAPAY_TRANSACTION_STATUS.CANCELLED,
        VERSAPAY_TRANSACTION_STATUS.COMPLETED_BUT_NSFED,
      ]

      if (transactionType === VERSAPAY_TRANSACTION_TYPE.DIRECT_DEBIT) {
        let chargeStatus
        if (versaPayFailedStatuses.includes(params.state)) {
          chargeStatus = PAYMENT_STATUS.CHARGE_FAILED
        } else if (params.state === VERSAPAY_TRANSACTION_STATUS.COMPLETED) {
          chargeStatus = PAYMENT_STATUS.CHARGED
        }
        if (chargeStatus !== trans.charge_status) {
          await knex('payments')
            .where('id', trans.id)
            .update({
              charge_status: chargeStatus,
              charge_status_details: params.state,
              updated_at: knex.raw('now()'),
            })
        }
      } else if (transactionType === VERSAPAY_TRANSACTION_TYPE.DIRECT_CREDIT) {
        let depositStatus
        if (versaPayFailedStatuses.includes(params.state)) {
          depositStatus = PAYMENT_STATUS.SEND_FAILED
        } else if (params.state === VERSAPAY_TRANSACTION_STATUS.COMPLETED) {
          depositStatus = PAYMENT_STATUS.SENT
        }
        if (depositStatus !== trans.deposit_status) {
          await knex('payments')
            .where('id', trans.id)
            .update({
              deposit_status: depositStatus,
              deposit_status_details: params.state,
              updated_at: knex.raw('now()'),
            })
        }
      }
    }

    ctx.status = 200
    ctx.body = {
      success: true,
    }
  }
}
