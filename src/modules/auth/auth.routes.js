
import Router from 'koa-router'
import passport from 'koa-passport'

import Controller from './auth.controller'
import { authMiddleware, tokenAuthMiddleware } from '../../common'

const router = new Router()
const controller = new Controller()

router.post('/login', (ctx) => {
  const { username, password } = ctx.request.body

  if (!username || !password) {
    ctx.throw(400, 'Missing username and/or password parameter')
  }

  return passport.authenticate('local', (err, user) => {
    if (!user) {
      ctx.body = {
        success: false,
      }
      ctx.throw(401)
    }
    ctx.body = {
      success: true,
      user: {
        id: user.id,
        firstName: user.first_name,
        lastName: user.last_name,
      },
    }
    return ctx.login(user)
  })(ctx)
})

router.get('/logout', (ctx) => {
  ctx.logout()
  ctx.status = 200
  ctx.body = {
    success: true,
  }
})

router.get('/session', async (ctx) => {
  await controller.session(ctx)
})

router.post('/user-exists', tokenAuthMiddleware, async (ctx) => {
  await controller.userExists(ctx)
})

router.post('/create-user', async (ctx) => {
  await controller.create(ctx)
})

router.post('/create-user-fb', tokenAuthMiddleware, async (ctx) => {
  await controller.createUserFacebook(ctx)
})

export default router
