import crypto from 'crypto'
import argon2 from 'argon2'

import knex from '../../database/connection'
import {
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH,
  SOCIAL_TYPE,
} from '../../config'


export default class AuthController {
  async create(ctx) {
    const { email, password, phone, firstName, lastName } = ctx.request.body

    if (!email || !password || !firstName || !lastName || !phone) {
      ctx.throw(400, 'Missing required parameters')
    }

    if (password.length < MIN_PASSWORD_LENGTH || password.length > MAX_PASSWORD_LENGTH) {
      ctx.throw(412, `Password has to be between ${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} characters`)
    }

    const foundUser = await knex('users').where('email', email).first('id')

    if (foundUser) {
      ctx.throw(412, `User ${email} already exists`)
    }

    const salt = await crypto.randomBytes(16)
    const hash = await argon2.hash(password, { salt })

    const user = await knex('users').returning('id').insert({
      email,
      password: hash,
      first_name: firstName,
      last_name: lastName,
      phone,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })

    ctx.status = 200
    ctx.body = {
      id: user[0],
    }
  }

  async createUserFacebook(ctx) {
    const { phone, firstName, lastName, fbUserId } = ctx.request.body

    if (!phone || !firstName || !lastName || !fbUserId) {
      ctx.throw(400, 'Missing required parameters')
    }

    const foundUser = await knex('users').where('phone', phone).first('id')

    if (foundUser) {
      ctx.throw(412, `User ${phone} already exists`)
    }

    const salt = await crypto.randomBytes(16)
    const password = await crypto.randomBytes(8)
    const hash = await argon2.hash(password, { salt })

    const user = await knex('users').returning('id').insert({
      first_name: firstName,
      last_name: lastName,
      phone,
      email: '',
      password: hash,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })

    if (!user[0]) {
      ctx.throw(500, 'Cannot create a user due to some problem')
    }

    const socialId = await knex('user_social_ids').returning('userId').insert({
      user_id: user[0],
      social_type: SOCIAL_TYPE.FB,
      userSocialId: fbUserId,
      created_at: knex.raw('now()'),
      updated_at: knex.raw('now()'),
    })

    if (!socialId[0]) {
      ctx.throw(500, 'Cannot link social id to the user due to some issue')
    }

    ctx.status = 200
    ctx.body = {
      id: user[0],
    }
  }

  async userExists(ctx) {
    const { socialType, userSocialId } = ctx.request.body

    if (!socialType || !userSocialId) {
      ctx.throw(400, 'Missing required parameters')
    }

    const socialUser = await knex('user_social_ids').where({
      social_type: SOCIAL_TYPE[socialType.toUpperCase()],
      user_social_id: userSocialId,
    }).first('user_id')

    if (!socialUser) {
      ctx.throw(404, 'User not found')
    } else {
      ctx.status = 200
    }
  }

  async session(ctx) {
    const { user } = ctx.state

    if (!user) {
      ctx.throw(401)
    }

    ctx.body = {
      user: {
        id: user.id,
        firstName: user.first_name,
        lastName: user.last_name,
      },
    }
  }
}
