import passport from 'koa-passport'
import LocalStrategy from 'passport-local'
import BearerStragery from 'passport-http-bearer'
import argon2 from 'argon2'

import knex from '../../database/connection'
import { SOCIAL_TYPE, EXTERNAL_API_TOKEN } from '../../config'

passport.serializeUser((user, done) => done(null, user.id))
passport.deserializeUser(async (id, done) => {
  try {
    const user = await knex('users').where('id', id).first(['id', 'first_name', 'last_name', 'email'])
    done(null, user || false)
  } catch (err) {
    done(err)
  }
})

passport.use(new LocalStrategy(async (username, password, done) => {
  let user
  try {
    user = await knex('users').where('email', username).first()
  } catch (e) {
    return done(null, false, { message: 'Internal error while trying to match user' })
  }
  if (!user) {
    return done(null, false, { message: 'Incorrect username or password.' })
  }
  const passwordMatch = await argon2.verify(user.password, password)
  if (!passwordMatch) {
    return done(null, false, { message: 'Incorrect username or password.' })
  }
  return done(null, user)
}))

passport.use(new BearerStragery({
  passReqToCallback: true,
}, async (req, token, done) => {
  const { header } = req

  if (token !== EXTERNAL_API_TOKEN) {
    return done(true, false, { message: 'Incorrect token provided' })
  }

  if (!header['x-social-type'] || !header['x-user-social-id']) {
    return done(true, false, { message: 'Missing required authorization parameters' })
  }

  let socialUser
  try {
    socialUser = await knex('user_social_ids').where({
      socialType: SOCIAL_TYPE[header['x-social-type'].toUpperCase()],
      user_social_id: header['x-user-social-id'],
    }).first()
  } catch (e) {
    return done(true, false, { message: 'Internal error while trying to match social user' })
  }
  if (!socialUser) {
    return done(true, false, { message: 'Social user not found' })
  }

  let user
  try {
    user = await knex('users').where('id', socialUser.user_id).first()
  } catch (e) {
    return done(true, false, { message: 'Internal error while trying to match user' })
  }
  if (!user) {
    return done(true, false, { message: 'User not found' })
  }
  return done(null, user)
}))
