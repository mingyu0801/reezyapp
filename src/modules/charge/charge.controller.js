import axios from 'axios'
import moment from 'moment'
import crypto from 'crypto'

import knex from '../../database/connection'
import {
  VERSAPAY_API_URL,
  VERSAPAY_API_KEY,
  VERSAPAY_API_TOKEN,
  VERSAPAY_FUND_TOKEN,
  PAYMENT_STATUS,
} from '../../config'


export default class ChargeController {
  async index(ctx) {
    const { paymentId } = ctx.request.body

    if (!paymentId) {
      ctx.throw(400, 'Missing required parameters')
    }

    const payment = await knex('payments').where('id', paymentId).andWhere(builder => {
      builder.whereNull('charge_status')
    }).first(['lease_id', 'amount'])

    if (!payment) {
      ctx.throw(412, 'The payment doesn\'t exist or already processed')
    }

    const lease = await knex('lease_agreements').where('id', payment.lease_id).first(['property_id', 'tenant_id'])
    if (!lease) {
      ctx.throw(412, 'The lease doesn\'t exist')
    }

    const property = await knex('properties').where('id', lease.property_id).first(['landlord_id', 'address'])
    if (!property) {
      ctx.throw(412, 'The property doesn\'t exist')
    }

    const paymentMethod = await knex('payment_methods').where('user_id', lease.tenant_id).first('id')
    if (!paymentMethod) {
      ctx.throw(412, 'The tenant doesn\'t have any payment method set up')
    }

    const versaPayToken = await knex('versapay_tokens').where('payment_method_id', paymentMethod.id).first('fund_token')
    if (!versaPayToken) {
      ctx.throw(412, 'The tenant doesn\'t have VersaPay fund token set up')
    }

    const tenant = await knex('users').where('id', lease.tenant_id).first()
    if (!tenant) {
      ctx.throw(412, 'The tenant information doesn\'t exist')
    }

    const uniqueReference = crypto.createHash('md5').update(`CHARGE_${lease.tenant_id}_${payment.lease_id}_${paymentId}_${process.env.NODE_ENV}`).digest('hex')
    let paymentSuccess = true
    let paymentError
    let versaPayResponse
    try {
      versaPayResponse = await axios({
        method: 'post',
        url: `${VERSAPAY_API_URL}/api/transactions`,
        data: {
          transaction_type: 'direct_debit',
          amount_in_cents: payment.amount * 100,
          email: tenant.email,
          message: `Rent payment for ${property.address}. #${paymentId}`,
          process_on: moment().format('YYYY-MM-DD'),
          unique_reference: uniqueReference,
          fund_token: VERSAPAY_FUND_TOKEN,
          from_fund_token: versaPayToken.fund_token,
          first_name: tenant.first_name,
          last_name: tenant.last_name,
          memo: `Rent for ${property.address}`,
          auto_withdraw: false,
        },
        auth: {
          username: VERSAPAY_API_TOKEN,
          password: VERSAPAY_API_KEY,
        },
      })
    } catch (error) {
      console.error(`VersaPay API direct_debit request failed (payment #${paymentId}):`, error.response.data)
      paymentSuccess = false
      paymentError = `VersaPay error response: ${error.response.status}`
    }

    if (!paymentSuccess) {
      await knex('payments').where('id', paymentId).update({
        charge_status: PAYMENT_STATUS.CHARGE_FAILED,
        statusDetails: paymentError,
      })
    } else {
      try {
        await knex('payments').where('id', paymentId).update('charge_status', PAYMENT_STATUS.CHARGE_IN_PROGRESS)
        await knex('versapay_transactions').insert({
          token: versaPayResponse.data.token,
          state: versaPayResponse.data.state,
          payment_id: paymentId,
          created_at: knex.raw('now()'),
          updated_at: knex.raw('now()'),
        })
      } catch (e) {
        console.error('Failed to log VersaPay transaction', e)
      }
    }

    ctx.status = 200
    ctx.body = {
      success: true,
    }
  }
}
