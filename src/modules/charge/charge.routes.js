
import Router from 'koa-router'
import Controller from './charge.controller'
import { internalUseMiddleware } from '../../common'

const router = new Router()
const controller = new Controller()

router.post('/', internalUseMiddleware, async (ctx) => {
  await controller.index(ctx)
})

export default router
