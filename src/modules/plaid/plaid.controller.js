import plaid from 'plaid'
import knex from '../../database/connection'
import {
  PLAID,
} from '../../config'

const plaidClient = new plaid.Client(
  PLAID.CLIENT_ID,
  PLAID.SECRET,
  PLAID.PUBLIC_KEY,
  PLAID.URL,
  { version: '2018-05-22' }
)


export default class PlaidController {
  async authPlaidItem(ctx) {
    const { publicToken } = ctx.request.body
    if (!publicToken) {
      ctx.throw(412, 'Missing publicToken')
    }
    const { user } = ctx.state
    const exist = await this.itemExist({ public_token: publicToken, user_id: user.id })
    if (exist) {
      ctx.throw(400, 'Item already exist')
    } else {
      let plaidItem = {}
      try {
        plaidItem = await plaidClient.exchangePublicToken(publicToken)
      } catch (error) {
        console.error('Auth Item in Plaid failed:', error)
        if (error.error_message) {
          ctx.throw(400, error.error_message)
        } else {
          ctx.throw(412, 'Auth Item in Plaid failed')
        }
      }
      // const plaidItem = await this.auth(ctx, publicToken)
      await knex('plaid_items').insert({
        user_id: user.id,
        access_token: plaidItem.access_token,
        item_id: plaidItem.item_id,
        public_token: publicToken,
        created_at: knex.raw('now()'),
        updated_at: knex.raw('now()'),
      })
      ctx.status = 200
      ctx.body = {
        success: true,
      }
    }
  }

  async getPlaidItem(ctx, params) {
    const plaidItems = await knex('plaid_items').where(params)
    return plaidItems[0]
  }

  async getPlaidAccounts(ctx) {
    const { publicToken } = ctx.params
    const { user } = ctx.state

    const plaidItem = await this.getPlaidItem(ctx, { public_token: publicToken, user_id: user.id })
    const { access_token } = plaidItem

    let plaidResponse
    try {
      plaidResponse = await plaidClient.getAuth(access_token)
    } catch (error) {
      console.error('Get Plaid accounts request failed:', error)
      if (error.error_message) {
        ctx.throw(400, error.error_message)
      } else {
        ctx.throw(412, 'Get Plaid accounts request failed')
      }
    }

    ctx.body = {
      success: true,
      accounts: plaidResponse.accounts,
    }
  }

  async itemExist(params = {}) {
    const plaidItem = await knex('plaid_items').where(params)
    return Boolean(plaidItem.length)
  }

  async getTransactions(ctx, { access_token, start_date, end_date }) {
    let plaidResponse
    try {
      plaidResponse = plaidClient.getTransactions(access_token, start_date, end_date)
    } catch (error) {
      console.error('Get Plaid transactions request failed:', error)
      if (error.error_message) {
        ctx.throw(400, error.error_message)
      } else {
        ctx.throw(412, 'Get Plaid transactions request failed')
      }
    }
    return plaidResponse
  }
}
