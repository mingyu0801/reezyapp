
import Router from 'koa-router'
import Controller from './plaid.controller'
import { authMiddleware } from '../../common'

const router = new Router()
const controller = new Controller()

router.post('/auth', authMiddleware, async (ctx) => {
  await controller.authPlaidItem(ctx)
})
router.get('/items/:publicToken/accounts', authMiddleware, async (ctx) => {
  await controller.getPlaidAccounts(ctx)
})

export default router
