
export function up(knex, Promise) {
  return knex.schema.createTable('lease_agreements', (table) => {
    table.increments('id');
    table.integer('property_id').notNullable();
    table.integer('tenant_id').notNullable();
    table.date('start_date').notNullable();
    table.date('end_date').notNullable();
    table.boolean('auto_extension').defaultTo(true);
    table.integer('rent_amount').notNullable();
    table.enu('billing_cycle', ['DAILY', 'WEEKLY', 'BI_WEEKLY', 'MONTHLY']).defaultTo('MONTHLY');
    table.enu('status', ['PENDING', 'ACTIVE', 'TERMINATED']).defaultTo('PENDING');
    table.timestamps(false, false);
  })
};

export function down(knex, Promise) {
  return knex.schema.dropTable('lease_agreements');
};
