
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user_social_ids', (table) => {
    table.integer('user_id').notNullable();
    table.string('social_type', 10).notNullable();
    table.string('user_social_id', 30).notNullable();
    table.timestamps(false, false);
    table.unique(['user_id', 'social_type']);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user_social_ids');
};
