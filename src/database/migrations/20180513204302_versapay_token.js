
exports.up = function(knex, Promise) {
  return knex.schema.createTable('versapay_tokens', (table) => {
    table.increments('id');
    table.integer('payment_method_id').notNullable();
    table.string('fund_token').notNullable();
    table.string('account_number').notNullable();
    table.timestamps(false, false);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('versapay_tokens');
};
