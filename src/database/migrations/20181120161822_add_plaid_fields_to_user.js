
exports.up = function(knex, Promise) {
  return knex.schema.createTable('plaid_items', (table) => {
    table.integer('user_id').primary().notNullable();
    table.string('item_id', 64).notNullable();
    table.string('access_token', 64).notNullable();
    table.string('public_token', 64).notNullable();
    table.timestamps(false, false);
  })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('plaid_items');
};
