
export function up(knex, Promise) {
  return knex.schema.createTable('properties', (table) => {
    table.increments('id');
    table.integer('landlord_id').notNullable();
    table.string('address').notNullable();
    table.timestamps(false, false);
  })
};

export function down(knex, Promise) {
  return knex.schema.dropTable('properties');
};
