
exports.up = function(knex, Promise) {
  return knex.schema.createTable('payment_methods', (table) => {
    table.increments('id');
    table.integer('user_id').notNullable();
    table.enu('type', ['EFT', 'CC']).defaultTo('EFT');
    table.timestamps(false, false);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('payment_methods');
};
