
exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', (table) => {
    table.increments('id');
    table.string('first_name').notNullable();
    table.string('last_name').notNullable();
    table.string('email').notNullable();
    table.string('password').notNullable();
    table.string('phone');
    table.timestamps(false, false);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users');
};
