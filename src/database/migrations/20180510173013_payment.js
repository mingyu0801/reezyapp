
exports.up = function(knex, Promise) {
  return knex.schema.createTable('payments', (table) => {
    table.increments('id');
    table.integer('lease_id').notNullable();
    table.integer('amount').notNullable();
    table.string('charge_status');
    table.string('charge_status_details');
    table.string('deposit_status');
    table.string('deposit_status_details');
    table.integer('attemps').defaultTo(0),
    table.timestamps(false, false);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('payments');
};
