
exports.up = function(knex, Promise) {
  return knex.schema.createTable('versapay_transactions', (table) => {
    table.increments('id');
    table.string('token').notNullable();
    table.string('state').notNullable();
    table.integer('payment_id');
    table.timestamps(false, false);
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('versapay_transactions');
};
