import moment from 'moment'
import crypto from 'crypto'
import argon2 from 'argon2'

export const seed = async (knex, Promise) => {
  await knex('payments').del()
  await knex('users').del()
  await knex('payment_methods').del()
  await knex('versapay_tokens').del()
  await knex('properties').del()
  await knex('lease_agreements').del()
  
  const salt = await crypto.randomBytes(16)
  const hash = await argon2.hash('12345678', { salt })

  const userIds = await knex('users').returning('id').insert([
    { first_name: 'Landlord 1', last_name: 'last_name', email: 'landlord1@test.com', password: hash },
    { first_name: 'Landlord 2', last_name: 'last_name', email: 'landlord2@test.com', password: hash },
    { first_name: 'Landlord 3', last_name: 'last_name', email: 'landlord3@test.com', password: hash },
    { first_name: 'Tenant 1', last_name: 'last_name', email: 'tenant1@test.com', password: hash },
    { first_name: 'Tenant 2', last_name: 'last_name', email: 'tenant2@test.com', password: hash },
    { first_name: 'Tenant 3', last_name: 'last_name', email: 'tenant3@test.com', password: hash },
  ]);

  const payment_method_ids = await knex('payment_methods').returning('id').insert([
    { user_id: userIds[0], type: 'EFT' },
    { user_id: userIds[1], type: 'EFT' },
    { user_id: userIds[2], type: 'EFT' },
    { user_id: userIds[3], type: 'EFT' },
    { user_id: userIds[4], type: 'EFT' },
    { user_id: userIds[5], type: 'EFT' },
  ]);

  await knex('versapay_tokens').insert([
    { payment_method_id: payment_method_ids[0], fund_token: 'token1', account_number: '1234' },
    { payment_method_id: payment_method_ids[1], fund_token: 'token2', account_number: '5678' },
    { payment_method_id: payment_method_ids[2], fund_token: 'token3', account_number: '9101' },
    { payment_method_id: payment_method_ids[3], fund_token: 'token4', account_number: '1213' },
    { payment_method_id: payment_method_ids[4], fund_token: 'token5', account_number: '1415' },
    { payment_method_id: payment_method_ids[5], fund_token: 'token6', account_number: '1617' },
  ])

  const propertyIds = await knex('properties').returning('id').insert([
    { landlord_id: userIds[0], address: '565 Wilson Ave' },
    { landlord_id: userIds[1], address: '564 Wilson Ave' },
    { landlord_id: userIds[2], address: '566 Wilson Ave' },
  ])

  await knex('lease_agreements').insert([
    {
      property_id: propertyIds[0],
      tenant_id: userIds[3],
      start_date: moment().utc().subtract(1, 'months').format('YYYY-MM-DD'),
      end_date: moment().utc().add(30, 'years').format('YYYY-MM-DD'),
      auto_extension: true,
      rent_amount: 1000,
      billing_cycle: 'MONTHLY',
      status: 'ACTIVE',
    },
    {
      property_id: propertyIds[1],
      tenant_id: userIds[4],
      start_date: moment().utc().subtract(5, 'days').format('YYYY-MM-DD'),
      end_date: moment().utc().add(30, 'years').format('YYYY-MM-DD'),
      auto_extension: true,
      rent_amount: 1500,
      billing_cycle: 'MONTHLY',
      status: 'ACTIVE',
    },
    {
      property_id: propertyIds[2],
      tenant_id: userIds[5],
      start_date: moment().utc().subtract(10, 'days').format('YYYY-MM-DD'),
      end_date: moment().utc().add(30, 'years').format('YYYY-MM-DD'),
      auto_extension: true,
      rent_amount: 2000,
      billing_cycle: 'MONTHLY',
      status: 'ACTIVE',
    }
  ])
}


