import passport from 'passport'
import { EXTERNAL_API_TOKEN } from './config'

export const authMiddleware = async (ctx, next) => {
  if (ctx.isAuthenticated()) {
    return next()
  }
  const { authorization } = ctx.request.header
  if (authorization) {
    return passport.authenticate('bearer', { session: false }, async (err, user, mess) => {
      if (!user) {
        ctx.throw(401)
      }
      await ctx.login(user)
      return next()
    })(ctx, next)
  }
  return ctx.throw(401)
}

export const internalUseMiddleware = (ctx, next) => {
  console.log('Client IP address', ctx.ip)
  if (['::1', '127.0.0.1', '::ffff:127.0.0.1'].includes(ctx.ip)) {
    return next()
  }
  return ctx.throw(403)
}

export const tokenAuthMiddleware = (ctx, next) => {
  const { authorization } = ctx.request.header

  if (!authorization || !/^Bearer (.)+$/.test(authorization)) {
    ctx.throw(401)
  }

  const token = authorization.split(' ')[1]
  if (token === EXTERNAL_API_TOKEN) {
    return next()
  }

  return ctx.throw(401)
}
