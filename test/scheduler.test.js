/* global describe, it, beforeAll, afterAll, expect */
/* eslint-disable import/first */
/* eslint-disable global-require */

process.env.NODE_ENV = 'test'

import moment from 'moment'

import knex from '../src/database/connection'
import { BILLING_CYCLE } from '../src/config'


describe('payment scheduler', () => {
  let dueLeases
  beforeAll(async (done) => {
    await knex.migrate.rollback()
    await knex.migrate.latest()
    await knex.seed.run()
    done()
  })
  afterAll(() => knex.migrate.rollback())

  describe('determine if rent is due to be paid', () => {
    const { whetherHasToPay } = require('../src/modules/scheduler/scheduler.controller')
    describe('daily cycle billing', () => {
      it('should return false when less than a day passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.MONTHLY,
        }, moment('2018-05-09'))).toBeFalsy()
      })
      it('should return false when more than a day passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.MONTHLY,
        }, moment('2018-05-11'))).toBeFalsy()
      })
      it('should return true when exactly one month passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.MONTHLY,
        }, moment('2018-05-10'))).toBeTruthy()
      })
    })
    describe('monthly cycle billing', () => {
      it('should return false when less than a month passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.MONTHLY,
        }, moment('2018-05-09'))).toBeFalsy()
      })
      it('should return false when more than a month passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.MONTHLY,
        }, moment('2018-05-11'))).toBeFalsy()
      })
      it('should return true when exactly one month passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.MONTHLY,
        }, moment('2018-05-10'))).toBeTruthy()
      })
    })
    describe('weekly cycle billing', () => {
      it('should return false when less than a week passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.WEEKLY,
        }, moment('2018-04-16'))).toBeFalsy()
      })
      it('should return false when more than a week passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.WEEKLY,
        }, moment('2018-04-18'))).toBeFalsy()
      })
      it('should return true when exactly one week passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.WEEKLY,
        }, moment('2018-04-17'))).toBeTruthy()
      })
    })
    describe('bi-weekly cycle billing', () => {
      it('should return false when less than two weeks passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.BI_WEEKLY,
        }, moment('2018-04-17'))).toBeFalsy()
      })
      it('should return false when more than two weeks passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.BI_WEEKLY,
        }, moment('2018-05-01'))).toBeFalsy()
      })
      it('should return true when exactly two weeks passed since a start date', () => {
        expect(whetherHasToPay({
          start_date: '2018-04-10',
          billing_cycle: BILLING_CYCLE.BI_WEEKLY,
        }, moment('2018-04-24'))).toBeTruthy()
      })
    })
  })

  it('should select leases that are due to be paid today', async (done) => {
    const { selectLeasesToBePaid } = require('../src/modules/scheduler/scheduler.controller')
    dueLeases = await selectLeasesToBePaid()
    expect(dueLeases.length).toBe(1)
    done()
  })

  describe('create tasks to charge', () => {
    let queueInstance
    beforeAll(async (done) => {
      queueInstance = require('./mocks/queueInstance.mock').default
      const { createChargeTasks } = require('../src/modules/scheduler/scheduler.controller')
      await createChargeTasks(dueLeases)
      done()
    })
    it('should call create method on the queue instance', () => {
      expect(queueInstance.create.mock.calls.length).toBe(1)
    })
    it('should call attempts method after create on the queue instance', () => {
      expect(queueInstance.attempts.mock.calls.length).toBe(1)
    })
    it('should pass value 3 to attempts method', () => {
      expect(queueInstance.attempts.mock.calls[0][0]).toBe(3)
    })
    it('should call backoff method after create on the queue instance', () => {
      expect(queueInstance.backoff.mock.calls.length).toBe(1)
    })
    it('should pass correct delay and type to the backoff method', () => {
      expect(queueInstance.backoff.mock.calls[0][0]).toEqual({
        delay: 3600 * 24 * 1000,
        type: 'fixed',
      })
    })
    it('should call save method after create on the queue instance', () => {
      expect(queueInstance.save.mock.calls.length).toBe(1)
    })
  })
})
