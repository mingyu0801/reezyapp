
/* global jest */

const QueueController = require('../../src/modules/queue/queue.controller')
QueueController.queueInstance = {
  create: jest.fn().mockReturnThis(),
  attempts: jest.fn().mockReturnThis(),
  backoff: jest.fn().mockReturnThis(),
  save: jest.fn(),
}

export default QueueController.queueInstance
