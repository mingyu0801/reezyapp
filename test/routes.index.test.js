/* global describe, it */
/* eslint-disable import/first */
process.env.NODE_ENV = 'test'

import supertest from 'supertest'
import app from '../src/server'

const request = supertest.agent(app.listen(3001))

describe('routes: ', () => {
  describe('GET /', () => {
    it('should return Reezy API', (done) => {
      request
        .get('/')
        .expect(200)
        .expect('Reezy API', done)
    })
  })

  // describe('POST /api/v1/charge', () => {
  //   it('should return run charge operation', (done) => {
  //     request
  //       .post('/api/v1/charge')
  //       .send({ a: 1 })
  //       .set('Accept', 'application/json')
  //       .set('Content-Type', 'application/json')
  //       .expect(412)
  //       .expect('Missing required parameters', done)
  //   })
  // })
})
