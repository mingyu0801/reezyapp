/* global describe, it, beforeAll, afterAll, beforeEach, afterEach, expect */
/* eslint-disable import/first */
/* eslint-disable global-require */

process.env.NODE_ENV = 'test'

import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import supertest from 'supertest'

import knex from '../src/database/connection'
import {
  selectLeasesToBePaid,
  createChargeTasks,
} from '../src/modules/scheduler/scheduler.controller'
import { PAYMENT_STATUS } from '../src/config'
import app from '../src/server'

// const request = supertest.agent(app.listen(3001))
// const mock = new MockAdapter()

xdescribe('charge module', () => {
  beforeEach(async (done) => {
    await knex.migrate.rollback()
    await knex.migrate.latest()
    await knex.seed.run()

    require('./mocks/queueInstance.mock')

    const dueLeases = await selectLeasesToBePaid()
    await createChargeTasks(dueLeases)

    done()
  })
  afterEach(() => knex.migrate.rollback())

  it('should return 400 if missing required params', () => {
    request
      .post('/api/v1/charge')
      .send({})
      .expect(400)
  })

  it('should return 412 if an invalid paymentId supplied', () => {
    request
      .post('/api/v1/charge')
      .send({ paymentId: 100 })
      .expect(412)
  })

  it('should update payment status to failed', (done) => {
    mock.onPost().reply(500)
    request
      .post('/api/v1/charge')
      .send({ paymentId: 1 })
      .expect(500, 'Internal server error at VersaPay')
      .end(async () => {
        const payment = await knex('payments').where('id', 1).first(['status', 'statusDetails'])
        expect(payment.status).toBe(PAYMENT_STATUS.CHARGE_FAILED)
        expect(payment.statusDetails).toBe('Internal server error at VersaPay')
        done()
      })
  })
})
