# Reezy API

The backend application

[API Documentation](https://documenter.getpostman.com/view/426616/RzZCDHAQ)

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/01a62d39db9908b65d92)

## Project core dependencies

### Environment

- [Node.js](https://nodejs.org/en/) (recommended version is **8.11.3**)
- [Yarn](https://yarnpkg.com/en/docs/install#mac-stable) (recommended version is **1.7.0**)
- [MySQL](https://www.mysql.com) (recommended version is **5.7**)
- [Redis](https://redis.io/)

### Node Libraries
- [Koa.js](https://koajs.com/) - Web framework
- [Passport](http://www.passportjs.org/) - Authorization strategories
- [Knex](https://knexjs.org/) - SQL Query builder, database migrations
- [Kue](https://github.com/Automattic/kue) - Job queue, stored in redis
- [Node-cron](https://github.com/kelektiv/node-cron) - Cron
- [Axios](https://github.com/axios/axios) - HTTP client
- [Moment](https://momentjs.com/) - Date/time functions

## Code style requirements

The code follows the standard [ECMAScript 2017](https://medium.freecodecamp.org/here-are-examples-of-everything-new-in-ecmascript-2016-2017-and-2018-d52fa3b5a70e). 

[ESLint](https://eslint.org/) is configured with rules (file `.eslint.js`) to catch most problems with code style, so make sure ESLint is installed globally (`npm i -g eslint`) on your machine and configured to work with your IDE, in this way you should see blocks of code highlighted if something is wrong with it.

## How to run the app

### Install node dependencies

*Run inside the project's folder*

```
yarn install
```

### Migrate the database
```
yarn run knex migrate:latest
```

### Run the dev server
```
yarn start
```

### Create a new migration
```
yarn run knex migrate:make [name]
```

### Environmental variables

Environmental variables have to be defined in `.env` file. 

The list of all available variables is provided in the `.env.sample`
